package com.personal.mark.mark;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.personal.mark.model.UserInfo;
import com.personal.mark.repository.UserInfoRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
class MarkApplicationTests {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
    private UserInfoRepository userInfoRepository;
	
	private UserInfo user;
	private List<UserInfo> userList;
	
	@Before
	public void setUp() {
	
		this.user = new UserInfo();
		this.user.setId("0bbc28ad-d24b-4fda-aed3-31c0dfda095d");
		this.user.setUsername("mark");
		this.user.setEmail("mark@gmail.com");
		this.user.setPassword("e10adc3949ba59abbe56e057f20f883e");
	
		userList = new ArrayList<>();
		userList.add(user);		
		
		Mockito.when(userInfoRepository.findAll())
		.thenReturn(userList);
		
	}
	
	
	@Test
	void getUserInfos() {
		UserInfo user = new UserInfo();
		user.setId("0bbc28ad-d24b-4fda-aed3-31c0dfda095d");
		user.setUsername("mark");
		user.setEmail("mark@gmail.com");
		user.setPassword("e10adc3949ba59abbe56e057f20f883e");
		
		List<UserInfo> userInfo = userInfoRepository.findAll();
		assertThat(userInfo.contains(user));
		
	}
	
	@After
	void GarbageObject() {
		userList.remove(user);
	}

	@Test
	void contextLoads() {
		
		try {
			this.mockMvc.perform(get("/api/user-info")).andDo(print())
			.andExpect(status().isOk())
	
            .andExpect(jsonPath("$.[0].id", is("0bbc28ad-d24b-4fda-aed3-31c0dfda095d")))
            .andExpect(jsonPath("$.username", is("mark")))
            .andExpect(jsonPath("$.password", is("e10adc3949ba59abbe56e057f20f883e")))
            .andExpect(jsonPath("$.email", is("mark@gmail.com")));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
