package com.personal.mark.Interface;

import java.util.List;

import com.personal.mark.model.UserInfo;

public interface UserInfoImpl {
	public List<UserInfo> getAllUserInfo();
}
