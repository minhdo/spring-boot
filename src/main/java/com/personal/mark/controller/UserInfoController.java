package com.personal.mark.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.personal.mark.Interface.UserInfoImpl;
import com.personal.mark.model.UserInfo;

@RestController
@RequestMapping("/api")
public class UserInfoController {
	@Autowired
	private UserInfoImpl userInfoService;
	
	@GetMapping("/user-info")
	public List<UserInfo> getUsers(){
		return userInfoService.getAllUserInfo();
	}
	
	

}
