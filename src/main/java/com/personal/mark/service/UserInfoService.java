package com.personal.mark.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.personal.mark.Interface.UserInfoImpl;
import com.personal.mark.model.UserInfo;
import com.personal.mark.repository.UserInfoRepository;

@Component
public class UserInfoService implements UserInfoImpl{
	
	@Autowired
	private UserInfoRepository UserInfoRepository;

	@Override
	public List<UserInfo> getAllUserInfo() {
		return UserInfoRepository.findAll();
	}

}
